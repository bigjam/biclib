# README #

### What is This? ###

사용자의 터치가 발생하였을때 등록된 CallBack함수들을 실행시킵니다.

UI터치와 겹쳤을때는 무시됩니다.

안드로이드 기기의 경우 BackKey에 대한 CallBack도 등록가능 합니다.

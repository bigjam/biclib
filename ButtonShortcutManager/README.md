# README #

### What is this repository for? ###

* UI.BUTTON 객체에 단축키를 부여합니다.

### How do I get set up? ###

* ButtonShortcutManager 를 객체에 추가합니다.
* ButtonShortcutInfo 필드에 버튼, 단축키 설정을 합니다.
* 버튼이 diable 상태이거나 다른 객체에 의해 가려져있을 경우 단축키는 동작하지 않습니다.
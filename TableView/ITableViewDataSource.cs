﻿using UnityEngine;
using System.Collections;
using BicDB.Container;
using System;

namespace BicUtil.TableView
{
    public interface ITableViewDataSource
    {
        int GetNumberOfCellsForTableView();
        float GetHeightForRowInTableView(int _row);
        TableRow GetCellForRowInTableView(int _row);
        IRecordContainer GetCellData(int _index, int _rowIndex, int _cellOrder);
        int GetCellCountInRow(int _rowIndex);
        int GetRowCount();
        int GetStartDataIndex(int _rowIndex);
        void ReloadData();
        int GetRowIndex(int _cellIndex);
        int GetRowIndex<T>(Func<T, bool> _find) where T : class, IRecordContainer;
        CollectionRowData GetArrangeInfoInRow(int _rowIndex);
        Func<CollectionRowData[]> ArrangeInfoBuilder{get;set;}
        int GetHeightUnit(int _rowIndex);
        void UpdateArrangeInfo();

        bool HasHeadRow{get;}
        bool HasFootRow{get;}
    }
}


﻿using UnityEngine;
#if BICUTIL_ADMOB
using System;
using BicUtil.Tween;
using GoogleMobileAds.Api;

namespace BicUtil.Ads
{
    public class AdmobBannerController : MonoBehaviour, IAdsBanner
    {   
        private static float reloadTime = 30f; 
        #region InstantData
        private BannerView bannerView;
        #endregion

        #region LifeCycle
        private void OnDestroy() {
            BicTween.Cancel(this.gameObject);
            AdsManager.Instance.RemoveBanner(this);
            bannerView.OnAdLoaded -= onLoaded;
            bannerView.OnAdFailedToLoad -= reloadBanner;
            bannerView.Destroy();
        }
        #endregion

        #region Logic
        private object adsPlacement;
        private Action<IAdsBanner> onLoadBannerAction;
        public void Load(string _unitId, object _adsPlacement, Action<IAdsBanner> _onLoadBannerAction, AdRequest _request){
            adsPlacement = _adsPlacement;
            onLoadBannerAction = _onLoadBannerAction;
            bannerView = new BannerView(_unitId, AdSize.Banner, AdPosition.Top);
            bannerView.OnAdFailedToLoad += reloadBanner;
            bannerView.OnAdLoaded += onLoaded;
            BicTween.Delay(0.1f).SubscribeComplete(()=>{
                bannerView.LoadAd(_request);
            }).SetTargetObject(this.gameObject);
        }

        private void onLoaded(object sender, EventArgs e)
        {
            reloadTime = 30f;
            BicTween.RunOnMainThread(()=>onLoadBannerAction(this));
        }

        private void reloadBanner(object sender, AdFailedToLoadEventArgs e)
        {
            BicTween.RunOnMainThread(reloadBanner);
        }


        private void reloadBanner(){
            reloadTime = Mathf.Min(reloadTime * 2f, 600f);
            BicTween.Delay(reloadTime).SubscribeComplete(()=>{
                var _log = "reload time is " + reloadTime.ToString();
                try{
                    if(adsPlacement == null){
                        _log += "/adsPlacement is null";
                    }

                    if(AdsManager.Instance == null){
                        _log += "/AdsManager.Instance is null";
                    }

                    if(this.gameObject == null){
                        _log += "/this.gameobejct is null";
                    }

                    AdsManager.Instance.CreateBanner(adsPlacement, Color.black, onLoadBannerAction);
                    _log += "/2";
                    Destroy(this.gameObject);
                    _log += "/3";
                }catch(System.Exception _e){
                    Debug.Log(_log);
                    throw new SystemException(_log);
                }
            }).SetTargetObject(this.gameObject);
        }
        #endregion

        #region IAdsBanner
        public void Destroy()
        {
            Destroy(this.gameObject);
        }

        public void Hide()
        {
            bannerView.Hide();
        }

        public bool IsReady()
        {
            return true;
        }

        public void SetPosition(Transform _parent, Vector2 _position)
        {
            this.transform.SetParent(_parent);
            this.transform.localPosition = _position;

            if(_parent.localPosition.y > 0){
                bannerView.SetPosition(AdPosition.Top);
            }else{
                bannerView.SetPosition(AdPosition.Bottom);
            }
        }

        public void Show()
        {
            bannerView.Show();
        }
        #endregion
    }
}
#else
namespace BicUtil.Ads
{
    public class AdmobBannerController : MonoBehaviour{

    }
}
#endif
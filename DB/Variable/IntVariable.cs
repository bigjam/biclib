﻿using System;
using System.Globalization;
using BicDB;
using BicDB.Storage;

namespace BicDB.Variable
{
	public class IntVariable : VariableBase, IVariable{
		#region AsValue
		virtual protected int data { get; set;}
		public int AsInt{ get{ return data; } set{ data = value; NotifyChanged ();} }

		public string AsString{ 
			get{ return AsInt.ToString (); } 
			set{
				AsInt = parse(value);
			} 
		}

		public float AsFloat{ get{ return (float)AsInt; } set{ AsInt = (int)value;} }
		public bool AsBool{ get{ return AsInt == 0 ? false : true; } set{ AsInt = (value ? 1 : 0) ;} }
		public DataType Type { get { return DataType.Int; }}

		public int AsIntWithoutNotify{
			set{data = value;}
			get{return data;}
		}
		#endregion

		#region LifeCycle
		public IntVariable() : base(){
			
		}

		public IntVariable(int _value) : base(){
			data = _value;
		}
		#endregion

		#region IDataBase
		public void BuildVariable(ref string _json, ref int _counter, IStringParser _parser)
		{
			_parser.BuildNumberVariable(this, ref _json, ref _counter);
		}

		public void BuildFormattedString(System.Text.StringBuilder _stringBuilder, IStringFormatter _formatter){
			_formatter.BuildFormattedString(this, _stringBuilder);
		}

		public IVariable AsVariable{ 
			get{ 
				return this;	
			} 
		}

		public D As<D>() where D : class, IDataBase{
			return this as D;
		}
		#endregion

		#region parser
		static internal int parse(string _value){
			try {
				return int.Parse(_value);
			} catch (Exception) {
				#if UNITY_EDITOR
				UnityEngine.Debug.LogWarning("[BicDB] int parse warning : " + _value);
				#endif
				return (int)float.Parse(System.Text.RegularExpressions.Regex.Replace(_value, "[^0-9.+-]", ""), CultureInfo.InvariantCulture.NumberFormat);
			}
		}
		#endregion
	}
}

